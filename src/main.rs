fn hello() {
    println!("Hello, GitLab 🦊!");
}

fn main() {
    hello();
}
